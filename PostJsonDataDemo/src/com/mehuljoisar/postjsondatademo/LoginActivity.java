package com.mehuljoisar.postjsondatademo;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class LoginActivity extends Activity {

	private static final String TAG = "LoginActivity";
	
	private Context mContext;
	private Intent mIntent;
	private ProgressDialog pdLoading;
	
	private class LoginTask extends AsyncTask<Void, Void, String>
	{
		
		private ArrayList<NameValuePair> mParams = new ArrayList<NameValuePair>();
		private JSONArray mJArray = new JSONArray();
		private JSONObject mJobject = new JSONObject();
		private String jsonString = new String();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pdLoading.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			
			try {
				mJobject.put("userName", "test");
				mJobject.put("password", "test");
				mJArray.put(mJobject);
				mParams.add(new BasicNameValuePair("message", mJArray.toString()));
				
				jsonString = WebAPIRequest.postJsonData("http://itriangletechnolabs.com/projects/ausa/login.php?", mParams);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			return jsonString;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pdLoading.dismiss();
			
			if(result!=null)
			{
				
/*				try {
					mJobject = new JSONObject(jsonString);
					if(mJobject.getString("Success").equals("True"))
					{
						mJArray = mJobject.getJSONArray("user");
						JSONObject mUser = mJArray.getJSONObject(0);
						
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				
				Log.e(TAG, jsonString);

			}
		}
		
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		initialization();
		
		new LoginTask().execute();
	}

	private void initialization() {
		mContext = this;
		mIntent = new Intent();
		pdLoading = new ProgressDialog(mContext);
		pdLoading.setMessage("loading...");
		
	}


}
